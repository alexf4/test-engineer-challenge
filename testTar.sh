#! /bin/sh

#This is my make shift script to test the tar command.

failedTest=0
totalTest=0

#Create a results file
time=`date`

filename=results.xml

#Get rid of the spaces
file=${time// /-}$filename

#Create the file
touch $file

echo   "<?xml version=\"1.0\" ?>" >> $file
echo "<?xml-stylesheet type=\"text/css\" href=\"test.css\"?>" >>$file

echo \<root\> >>$file


#Create all the formats------------------------------------------------------------------------

#Test one, create a tar
echo \<Test\> >> $file
echo \<title\> Test: "tar -czf file.tar data.txt Alex\ Kharbush\ -\ Resume.pdf" \</title\> >> $file
totalTest=$((totalTest+1))
tar -czf file.tar data.txt Alex\ Kharbush\ -\ Resume.pdf 


#check if file exists
if [ -s file.tar ]; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> file.tar created \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> file.tar not created \</actions\> >> $file
    failedTestArr[$failedTest]="tar -czf file.tar data.txt Alex\ Kharbush\ -\ Resume.pdf"
    failedTest=$((failedTest+1))
fi 

#remove the file
rm file.tar

echo \</Test\> >> $file

#Test Two, create a pax
echo \<Test\> >> $file
echo \<title\> Test:"tar -czf file.pax data.txt Alex\ Kharbush\ -\ Resume.pdf " \</title\> >> $file
totalTest=$((totalTest+1))

tar -czf file.pax data.txt Alex\ Kharbush\ -\ Resume.pdf 


#check if file exists
if [ -s file.pax ]; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> file.pax created \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> file.pax not created \</actions\> >> $file
    failedTestArr[$failedTest]="tar -czf file.pax data.txt Alex\ Kharbush\ -\ Resume.pdf"
    failedTest=$((failedTest+1))

fi 

echo \</Test\> >> $file

#remove the file
rm file.pax

#Test Three, create a cpio
echo \<Test\> >> $file
echo \<title\> Test: "tar -czf file.cpio data.txt Alex\ Kharbush\ -\ Resume.pdf" \</title\> >> $file

totalTest=$((totalTest+1))
tar -czf file.cpio data.txt Alex\ Kharbush\ -\ Resume.pdf


#check if file exists
if [ -s file.cpio ]; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> file.cpio created \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> file.cpio not created \</actions\> >> $file
    failedTestArr[$failedTest]="tar -czf file.cpio data.txt Alex\ Kharbush\ -\ Resume.pdf"
    failedTest=$((failedTest+1))
fi 

echo \</Test\> >> $file

#remove the file
rm file.cpio

#Test Four, create a ar
echo \<Test\> >> $file
echo \<title\> Test: "tar -czf file.ar data.txt Alex\ Kharbush\ -\ Resume.pdf " \</title\> >> $file
totalTest=$((totalTest+1))

tar -czf file.ar data.txt Alex\ Kharbush\ -\ Resume.pdf  


#check if file exists
if [ -s file.ar ]; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> file.ar created \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> file.ar not created \</actions\> >> $file
    failedTestArr[$failedTest]="tar -czf file.ar data.txt Alex\ Kharbush\ -\ Resume.pdf"
    failedTest=$((failedTest+1))
fi 

echo \</Test\> >> $file

#remove the file
rm file.ar

#Test Five, create a shar
echo \<Test\> >> $file
echo \<title\> Test: "tar -czf file.shar data.txt Alex\ Kharbush\ -\ Resume.pdf"   \</title\> >> $file
totalTest=$((totalTest+1))

tar -czf file.shar data.txt Alex\ Kharbush\ -\ Resume.pdf 


#check if file exists
if [ -s file.shar ]; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> file.shar created \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> file.shar not created \</actions\> >> $file
    failedTestArr[$failedTest]="tar -czf file.shar data.txt Alex\ Kharbush\ -\ Resume.pdf"
    failedTest=$((failedTest+1))
fi 

echo \</Test\> >> $file


#remove the file
rm file.shar


#list the contents of an tar =============================================================
echo \<Test\> >> $file
echo \<title\> Test:"tar -tf file.tar"  \</title\> >> $file
totalTest=$((totalTest+1))
files="data.txt README"

tar -czf file.tar $files

contents=`tar -tf file.tar`

format=`echo $contents`

compare="data.txt README"

if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> file.tar contents are correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> file.tar contents are incorrect $format does not equal $compare \</actions\> >> $file
    failedTestArr[$failedTest]="tar -tf file.tar"
    failedTest=$((failedTest+1))
fi
echo \</Test\> >> $file

rm file.tar

#Extract contents ======================================================================================
echo \<Test\> >> $file
echo \<title\> Test:"tar -xf file.tar" \</title\> >> $file
totalTest=$((totalTest+1))
#Create a file
touch tempFile

#Put some data into the file
echo hello >> tempFile

#Create a tar file
tar -czf file.tar tempFile

#Remove the tempFile
rm tempFile

#Unpack the tar file
tar -xf file.tar

#Check to see if the file is made
if [ -s tempFile ]; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> tempFile unarchived \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> tempFile not created \</actions\> >> $file
    failedTestArr[$failedTest]="tar -xf file.tar"
    failedTest=$((failedTest+1))
fi 

echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"[[ $linevar = hello ]]"  \</title\> >> $file
totalTest=$((totalTest+1))
#Check the contents of the file
while read linevar
do
if [[ $linevar = hello ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> tempFile contents are correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> tempFile contents are incorrect\</actions\> >> $file
    failedTestArr[$failedTest]="[[ $linevar = hello ]]"
    failedTest=$((failedTest+1))    
fi

done < tempFile


echo \</Test\> >> $file
#Remove the file
rm tempFile

#Remove the Tar
rm file.tar 

#Create all the appeding ========================================================

#Create fail here

echo \<Test\> >> $file
echo \<title\> Test:"tar --append --file=file.tar README"  \</title\> >> $file
totalTest=$((totalTest+1))
#Create an archive
tar -czf file.tar data.txt

#Append another file to the archive
tar -cf temp.tar README @file.tar

contents=`tar -tf temp.tar`

format=`echo $contents`

compare="README data.txt"

#Check the contents of the new archive


if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> temp.tar contents are correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> temp.tar contents are incorrect $format does not equal $compare \</actions\> >> $file
    failedTestArr[$failedTest]="tar --append --file=file.tar README"
    failedTest=$((failedTest+1))
fi

rm file.tar

rm temp.tar

echo \</Test\> >> $file


#Test the Options ================================================================================

echo \<Test\> >> $file
echo \<title\> Test:"tar --exclude 'README' -cf temp.tar README data.txt"  \</title\> >> $file
totalTest=$((totalTest+1))
#Create the tar with the exclude 
tar --exclude 'README' -cf temp.tar README data.txt

#Check the contents of tar
contents=`tar -tf temp.tar`

format=`echo $contents`

compare="data.txt"

if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> temp.tar contents are correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> temp.tar contents are incorrect $format does not equal $compare \</actions\> >> $file
    failedTestArr[$failedTest]="tar --exclude 'README' -cf temp.tar README data.txt"
    failedTest=$((failedTest+1))
fi

#remove the temp tar
rm temp.tar


echo \</Test\> >> $file


echo \<Test\> >> $file
echo \<title\> Test:"tar --include 'README' -cf temp.tar "  \</title\> >> $file
totalTest=$((totalTest+1))
tar -cf old.tgz data.txt README

#create a tar file and use the include tool
tar --include='README' -cf include.tar  @old.tgz


#Check the contents of tar
contents=`tar -tf include.tar`

format=`echo $contents`

compare="README"

if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> include.tar contents are correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> include.tar contents are incorrect $format does not equal $compare \</actions\> >> $file
    failedTestArr[$failedTest]="tar --include 'README' -cf temp.tar"
    failedTest=$((failedTest+1))
fi


rm old.tgz
rm include.tar

echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar -cjf bzip.tar.bz2 data.txt README"  \</title\> >> $file
totalTest=$((totalTest+1))
#Create the bzip tar
tar -cjf bzip.tar.bz2 data.txt README

#Get the contents
contents=`tar -tf bzip.tar.bz2`

format=`echo $contents`

compare="data.txt README"

if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> temp.tar contents are correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> temp.tar contents are incorrect $format does not equal $compare \</actions\> >> $file
    failedTestArr[$failedTest]="tar -cjf bzip.tar.bz2 data.txt README"
    failedTest=$((failedTest+1))
fi

#remove the temp tar
rm bzip.tar.bz2


echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar -xkf temp.tar"  \</title\> >> $file
totalTest=$((totalTest+1))
#Create a file that will be deleted after it is added to the arc    
touch jumper

#Create an archive with duplicates
tar -cf duplicates.tar README data.txt jumper

rm jumper

tar -xkf duplicates.tar 


#test to make sure that jumper was un archived and README and data.txt was not archived
#Check to see if the file is made
if [ -e jumper ]; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> jumper unarchived \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> jumper not created \</actions\> >> $file
    failedTestArr[$failedTest]="tar -xkf temp.tar"
    failedTest=$((failedTest+1))
fi 

rm jumper

rm duplicates.tar


echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar -xmf temp.tar"  \</title\> >> $file
totalTest=$((totalTest+1))
#Create a file
touch delorean

#Set the mod time
touch -t 200701310846.26 delorean

#Create the archive
tar -cf time.tar delorean

#rm delorean

#Extract the time arc contents
tar -xmf time.tar

#check the modification time
#find files that have not been modded in 100 + days

#result=`find . -mtime +100`

result=`ls -1 -rt | tail -2| head -1`


if [[ $result = delorean ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> delorean mod time is correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> delorean mod time is incorrect \</actions\> >> $file
    failedTestArr[$failedTest]="tar -xmf temp.tar"
    failedTest=$((failedTest+1))
fi

rm delorean

rm time.tar


echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar -cfn temp.tar tempDir README"  \</title\> >> $file
totalTest=$((totalTest+1))
#Create a directory
mkdir recur

#add a file to that directory
cd recur

touch sive

cd ..

#archive that dir
tar -cnf recursive.tar recur

#Delete the dir
rm -r recur


#check the contest of dir
tar -xf recursive.tar

files=`ls recur`

if [[ $files = "" ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> reccur dir is empty \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> reccur dir is not empty \</actions\> >> $file
    failedTestArr[$failedTest]="tar -cfn temp.tar tempDir README"
    failedTest=$((failedTest+1))
fi


rm -r recur

rm recursive.tar

echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar --newer -cf temp.tar "  \</title\> >> $file
totalTest=$((totalTest+1))
mkdir timeCapsule

mv README timeCapsule

cd timeCapsule

#Create a file
touch delorean

#Create a tar with files only newer then now
tar --newer="Mar 27 19:57:48 2011" -cf newer.tar README delorean

#list the contents of the arc
contents=`tar -tf newer.tar`

format=`echo $contents`

compare="delorean"

mv README ..

cd ..

if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> reccur dir is empty \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> reccur dir is not empty \</actions\> >> $file
    failedTestArr[$failedTest]="tar --newer -cf temp.tar "
    failedTest=$((failedTest+1))
fi

rm -r timeCapsule


echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar --newer-mtime -cf temp.tar"  \</title\> >> $file
    
totalTest=$((totalTest+1))
#Create a file

touch delorean

#Change its mtime
touch -t 201301310846.26 delorean

#Create an arch that holds the new file using newer

tar --newer-mtime="Mar 27 19:57:48 2012" -cf mtime.tar delorean data.txt

#Compare the contents of the arch

contents=`tar -tf mtime.tar`

format=`echo $contents`

compare="delorean"

if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> reccur dir is empty \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> reccur dir is not empty \</actions\> >> $file
    failedTestArr[$failedTest]="tar --newer-mtime -cf temp.tar"
    failedTest=$((failedTest+1))
fi


rm delorean
rm mtime.tar

echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar --newer-than -cf temp.tar"  \</title\> >> $file
totalTest=$((totalTest+1))
#This test works when hand typed in the command line, but not in the script
#Create two files

touch firstFile
touch secondFile

#Create an arch that includes files newer then the first file created

tar -cf newerthan.tar --newer-than firstFile firstFile secondFile data.txt

#Comprare the contents of the arch

contents=`tar -tf newerthan.tar`

format=`echo $contents`

compare="secondFile"

if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> reccur dir is empty \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> reccur dir is not empty \</actions\> >> $file
    failedTestArr[$failedTest]="tar --newer-than -cf temp.tar"
    failedTest=$((failedTest+1))
fi

rm firstFile
rm secondFile
rm newerthan.tar

echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar --newer-mtime-than -cf temp.tar"  \</title\> >> $file
totalTest=$((totalTest+1))
#This test works when hand typed in the command line, but not in the script

#Create two files
touch firstFile
touch secondFile

#Create an arch that includes files newer then the first file created

tar -cf newerthan.tar --newer-mtime-than firstFile firstFile secondFile data.txt
#tar -cf newerthanmtime.tar  firstFile secondFile data.txt 

#Comprare the contents of the arch

contents=`tar -tf newerthanmtime.tar`

echo $contents

format=`echo $contents`

compare="secondFile"

if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> newerthanmtime.tar is correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> newerthanmtime.tar is incorrect \</actions\> >> $file
    
    failedTestArr[$failedTest]="tar --newer-mtime-than -cf temp.tar"
    failedTest=$((failedTest+1))
    
fi

rm firstFile
rm secondFile
rm newerthan.tar


echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar -xfO temp.tar"  \</title\> >> $file
totalTest=$((totalTest+1))
#Create a hello.txt file
touch hello.txt

#Append hello to the hello.txt file
echo hello > hello.txt

#Arch the file
tar -cf hello.tar hello.txt

#Write the file to output using tar command O
output=`tar -xOf hello.tar`

if [[ $output = hello ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> reccur dir is empty \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> reccur dir is not empty \</actions\> >> $file
    failedTestArr[$failedTest]="tar -xfO temp.tar"
    failedTest=$((failedTest+1))
fi

rm hello.tar
rm hello.txt

echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar -xfT FILES"  \</title\> >> $file
totalTest=$((totalTest+1))
#Create a file
touch files 
touch data2.txt

#Add filenames to the file
echo "data2.txt" > files

#Create the archive
tar -cf xfiles.tar data.txt data2.txt

rm data2.txt

#Xtract the file that is listed in the text file
tar -xT files -f xfiles.tar

#see if the file exists
if [ -e data2.txt ]; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> data2.txt  unarchived \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> data2.txt  not created \</actions\> >> $file
    failedTestArr[$failedTest]="tar -xfT FILES"
    failedTest=$((failedTest+1))
fi 

#see if the other file does not exist
if [ -e "data.txt 2" ]; then
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> data.txt 2 unarchived \</actions\> >> $file
    failedTestArr[$failedTest]=" -e data.txt 2" 
    failedTest=$((failedTest+1))
else
    echo \<result\> Succes: \</result\> >> $file
    echo \<actions\> data.txt 2 not created \</actions\> >> $file
fi 

rm data2.txt
rm files
rm xfiles.tar

echo \</Test\> >> $file

echo \<Test\> >> $file
echo \<title\> Test:"tar -cT files -f xfiles.tar "  \</title\> >> $file
totalTest=$((totalTest+1))
#Create a txt file to hold the name of the files
touch files
echo "data.txt" > files

tar -cT files -f xfiles.tar 

#check the insides of xfiles
contents=`tar -tf xfiles.tar`

format=`echo $contents`

compare="data.txt"

#Check the contents of the new archive


if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> xfiles.tar contents are correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> xfiles.tar contents are incorrect $format does not equal $compare \</actions\> >> $file
    failedTestArr[$failedTest]="tar -cT files -f xfiles.tar "
    failedTest=$((failedTest+1))
fi

rm xfiles.tar
rm files

echo \</Test\> >> $file


echo \<Test\> >> $file
echo \<title\> Test:"tar -X instructions"  \</title\> >> $file
totalTest=$((totalTest+1))

touch instructions

echo README > instructions

tar -cf instructions.tar -X instructions README data.txt

contents=`tar -tf instructions.tar`

format=`echo $contents`

compare="data.txt"

#Check the contents of the new archive


if [[ $format = $compare ]] ; then
    echo \<result\> Success: \</result\> >> $file
    echo \<actions\> instructions.tar contents are correct \</actions\> >> $file
else
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> instructions.tar contents are incorrect $format does not equal $compare \</actions\> >> $file
    failedTestArr[$failedTest]="tar -X instructions"
    failedTest=$((failedTest+1))
fi

rm instructions
rm instructions.tar

echo \</Test\> >> $file


echo \<Test\> >> $file
echo \<title\> Test:"tar --keep-newer-files -x temp.tar"  \</title\> >> $file
totalTest=$((totalTest+1))


#Create a temp file
touch keep

#Create an archive of the new file we just made
tar -cf knf.tar keep

rm keep

touch keep

tar --keep-newer-files -xf knf.tar

#see if the other file does not exist
if [ -e "keep 2" ]; then
    echo \<result\> Fail: \</result\> >> $file
    echo \<actions\> keep unarchived \</actions\> >> $file
    failedTestArr[$failedTest]="tar --keep-newer-files -x temp.tar"
    failedTest=$((failedTest+1))
else
    echo \<result\> Succes: \</result\> >> $file
    echo \<actions\> keep not created \</actions\> >> $file
    
fi 

rm knf.tar
rm keep


echo \</Test\> >> $file


#Add the end tag
echo \</root\> >>$file


#Display the results=============================================================================================


echo "Total Tests:"$totalTest
echo "Test Fails:" $failedTest
echo "Found Failed Tests:"

for ((x=0; x<$failedTest; x++)); do
echo ${failedTestArr[${x}]}
done


echo "Would you like to view the total results? (Y/N)"
read firstResponse
if [[ $firstResponse = "Y" || $firstResponse = "y" ]]; then

    array=( $(grep -o "<title>.*<.title>" $file | sed -e "s/^.*<title/<title/" | cut -f2 -d">"| cut -f1 -d"<"))

    cat -n $file

fi

echo "would you like to view results in Safari?(Y/N)"

read response

if [[ $response = "Y" || $response = "y" ]]; then


    #open the website 
    open -a Safari $file

fi
